"""
    Author: Izel Furuncuoglu
"""
class History(object):
    """Holds history structure"""

    def __repr__(self):
        return str((self.prepretag, self.pretag, self.tag, self.word))

    def __init__(self, prepretag, pretag, tag, word):
        self.prepretag = prepretag
        self.pretag = pretag
        self.tag = tag
        self.word = word
