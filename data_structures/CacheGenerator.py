"""
    Author: Izel Furuncuoglu
"""
from data_structures.Tag import *


class CacheGenerator(object):
    @staticmethod
    def generate_feature_cache(token, feature_keys_generator):
        """Create a feature cache"""
        feature_cache = {}
        index = 0

        for history in token:
            feature_keys = feature_keys_generator(history)
            for feature_key in feature_keys:
                if feature_key not in feature_cache:
                    if index % 5000 == 0:
                        print("%d features created." % index)

                    feature_cache[feature_key] = index
                    index = index + 1


        return feature_cache

    @staticmethod
    def generate_history_cache(token, feature_keys_generator, feature_cache):
        """Create a history cache"""
        history_cache = {}
        for history in token:
            key = (history.prepretag, history.pretag, history.tag, history.word)
            feature_keys = feature_keys_generator(history)

            if key not in history_cache:
                history_cache[key] = [feature_cache[feature_key] for feature_key in feature_keys]

        print("History cache is created with %d items. Originally the number of words was %d." % (len(history_cache),len(token)))
        return history_cache

    @staticmethod
    def generate_index_matrix(token, feature_keys_generator, feature_cache):
        """Create a matrix of indexes for each x and y. """
        matrix_cache = []
        y_indexes = []
        for history in token:
            ys = []
            original_y = history.tag
            for idx, tag in enumerate(Tag):
                if tag == original_y:
                    y_indexes.append(idx)
                history.tag = tag
                key = (history.prepretag, history.pretag, history.tag, history.word)
                feature_keys = feature_keys_generator(history)
                index_ls = [feature_cache[feature_key] for feature_key in feature_keys if feature_key in feature_cache]
                # if (key,tag) not in history_cache:
                #     history_cache[(key, tag)] = index_ls
                ys.append(index_ls)
            matrix_cache.append(ys)

        return (matrix_cache, y_indexes)
