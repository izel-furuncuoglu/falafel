"""
    Author: Izel Furuncuoglu
"""
from data_structures.History import *


class Helper(object):
    @staticmethod
    def parse(str):
        """Returns an array of histories"""
        pretag = None
        prepretag = None
        elems = str.split()
        parsed = []
        for idx, elem in enumerate(elems):
            if idx == 1:
                pretag = elems[0].split("_")[1]

            elif idx > 1:
                prepretag = elems[idx - 2].split("_")[1]
                pretag = elems[idx - 1].split("_")[1]

            word_tag = elem.split("_")
            parsed.append(History(prepretag, pretag, word_tag[1], word_tag[0]))

        return parsed

    @staticmethod
    def file_read(file_path):
        with open(file_path, "r") as filename:
            return filename.read()

    @staticmethod
    def file_write(file_path, text):
        with open(file_path, "w") as filename:
            filename.write(text)
