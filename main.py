from data_structures.Helper import *
from data_structures.CacheGenerator import *
from test_case_generator import TEST_CASES


def main():
    text = Helper.file_read("data/train.wtag")

    feature_keys_generator = TEST_CASES[0]

    # This function parses text and creates history items.
    token = Helper.parse(text)
    # Feature cache holds <feature_key:index>
    feature_cache = CacheGenerator.generate_feature_cache(token, feature_keys_generator)
    # History cache holds <history: array of indexes where there is 1 in feature vector>
    history_cache = CacheGenerator.generate_history_cache(token, feature_keys_generator, feature_cache)

    # this creates a matrix that contains for each x and y => the list of indexes
    # y_indexes contain the position of the real label in each feature vector.
    matrix, y_indexes = CacheGenerator.generate_index_matrix(token, feature_keys_generator, feature_cache)

    # If you want to store feature cache comment out below.
    # Helper.file_write("feature_cache.txt", str(feature_cache))

    # If you want to store history cache comment out below.
    # Helper.file_write("history_cache.txt", str(history_cache))

    # If you want to store matrix comment out below.
    # Helper.file_write("matrix.txt", str(matrix))

    # If you want to store y indexes comment out below.
    # Helper.file_write("y_indexes.txt", str(y_indexes))
    print(feature_cache)

if __name__ == "__main__":
    main()
