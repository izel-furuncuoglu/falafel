import math
import numpy as np
import scipy.optimize as sc


class TrainingModel(object):
    def scalar_v_features(self, v, features, i, j):
        return sum(v[k] for k in features[i][j])

    def loss_function(self, v, features, idx_labels, lambda_term):
        features = np.array(features)
        linear_term, normalization_term = 0, 0
        for i in range(len(features)):
            i_label = idx_labels[i]
            linear_term += self.scalar_v_features(v, features, i_label, i_label)
            normalization_term += math.log(
                sum(
                    np.array(
                        [
                            math.exp(self.scalar_v_features(v, features, i, j)) for j in range(features.shape[1])
                        ]
                    )
                )
            )
        print(normalization_term)
        regularization_term = lambda_term / 2 * sum(v)
        return linear_term - normalization_term - regularization_term

    def probability(self, v, features, i, j):
        return math.exp(self.scalar_v_features(v, features, i, j)) / (sum(np.array(math.exp(self.scalar_v_features(v, features, i, h))) for h in range(features.shape[1])))

    def gradient_k_function(self, v, k, features, idx_labels, lambda_term):
        linear_term, normalization_term = 0, 0
        linear_term = np.sum(np.array([1 if k in features[i][idx_labels[i]] else 0 for i in range(len(features))]))
        for i in range(len(features)):
            for j in range(len(features)):
                if k in features[i][j]:
                    normalization_term += features[i][j] * self.probability(v, features, i, j)
        regularization_term = lambda_term * v[k]
        return linear_term - normalization_term - regularization_term

    def generate_gradient(self, v, features, idx_labels, lambda_term):
        gradient = []
        for k in range(2):
            gradient.append(self.gradient_k_function(v, k, features, idx_labels, lambda_term))
        return np.array(gradient)

    def generate_optimal_v(self, v, features, idx_labels, lambda_term):
        return sc.fmin_l_bfgs_b(
            func=self.loss_function,
            args=(features, idx_labels, lambda_term),
            x0=v,
            fprime=self.generate_gradient
        )

