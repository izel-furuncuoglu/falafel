"""This file uses combinations of functions and exports them for selecting best features."""
from data_structures.Tag import *

feature_keys_generator = lambda history: [("f100", history.word, history.tag),
                                          ("f103", history.prepretag, history.pretag, history.tag),
                                          ("f104", history.pretag, history.tag),
                                          ("f105", history.tag),
                                          ]

# Generate test cases
TEST_CASES = [
    feature_keys_generator
]
