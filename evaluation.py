"""
CONFUSION MATRIX
Author: Yohan LEVY
Team: Falafel
"""

import pandas as pd
import numpy as np

def confusion_matrix(y_true, y_pred):
    """ Plot confusion matrix and returns the accuracy """
    labels = np.unique(y_true + y_pred)
    Nlabels = len(labels)
    cfn_matrix = pd.DataFrame(
        data=np.zeros((Nlabels, Nlabels)),
        index=labels,
        columns=labels
    )
    N = len(y_true)
    for i in range(N):
        cfn_matrix[y_true[i]][y_pred[i]] += 1
    print(cfn_matrix)
    accuracy = 0
    for label in labels:
        accuracy += cfn_matrix[label][label]
    print('ACCURACY:', accuracy / N)

confusion_matrix(['N', 'V', 'A', 'V', 'V'], ['N', 'N', 'N', 'V', 'A'])
