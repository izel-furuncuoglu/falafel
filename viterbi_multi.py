"""
VITERBI
Author: Yohan LEVY
Team: Falafel
"""

import numpy as np
from multiprocessing import Pool


TAGS = ['N', 'V', 'A', '.', 'NN', 'VP']
NTAGS = len(TAGS)
SEN = 'The dog is barking too loud ITC fellows'
SEN2 = 'I am a thug life'
NSEN = len(SEN.split())
NSEN2 = len(SEN2.split())


def compute_probs(t, h):
    return np.random.rand()


def Viterbi(nsen):
    """ MAIN """
    idx_tags = np.ones(nsen)
    PI = np.ones((NTAGS, NTAGS, nsen))
    BP = np.ones(PI.shape)
    for k in range(1, nsen):
        for idx_u, u in enumerate(TAGS):
            for idx_v, v in enumerate(TAGS):
                vect = PI[idx_u, :, k - 1]
                pvect = np.random.rand(*vect.shape)
                res = np.multiply(vect, pvect)
                argmax = np.argmax(res)
                PI[idx_u, idx_v, k] = res[argmax]
                BP[idx_u, idx_v, k] = argmax
    k = nsen - 1
    mat = PI[:, :, k]
    id_u, id_v = divmod(np.argmax(mat), int(mat.shape[0]))
    idx_tags[k] = int(id_v)
    idx_tags[k - 1] = int(id_u)
    for k in range(nsen - 3, -1, -1):
        idx_tags[k] = BP[int(idx_tags[k + 1]), int(idx_tags[k + 2]), k + 2]
    print([TAGS[int(i)] for i in idx_tags])


if __name__ == '__main__':
    pool = Pool()
    args = [NSEN, NSEN2, NSEN, NSEN, NSEN]
    pool.map(Viterbi, args)
    #Viterbi(NSEN)
